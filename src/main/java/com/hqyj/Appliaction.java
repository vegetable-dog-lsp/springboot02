package com.hqyj;
import javafx.application.Application;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

/**
 * @Auther: ZLF
 * @Date: 2021/12/13 10:20
 * @Description:
 */
//扫描controller；service；mapper

@SpringBootApplication
@MapperScan("com.hqyj.*.mapper")
public class Appliaction {

    //静态常量
    private static final Logger LOG = LoggerFactory.getLogger(Appliaction.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Appliaction.class);
        Environment env = app.run(args).getEnvironment();
        LOG.info("启动成功！！！");
        LOG.info("地址：\thttp://127.0.0.1:{}", env.getProperty("server.port"));

    }
}