package com.hqyj.system.controller;

import com.hqyj.utils.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Auther: ZLF
 * @Date: 2021/12/16 09:22
 * @Description:
 */
@Controller
@RequestMapping("/index")
public class IndexController extends BaseController {

    @RequestMapping("/top")
    public String top() {
        return "view/frame/top";
    }
    @RequestMapping("/toIndex")
    public String toIndex() {
        return "view/frame/index";
    }
    @RequestMapping("/left")
    public String left() {
        return "view/frame/left";
    }
    @RequestMapping("/right")
    public String right() {
        return "view/frame/right";
    }

}