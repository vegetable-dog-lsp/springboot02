package com.hqyj.system.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
    @RequestMapping("toLogin")
    /**
     *跳转到登陆页面
     */
    public String toLogin(){

        return "login";
    }
    @RequestMapping("login")
    /**
     * 执行具体的方法
     * username 用户名
     * password 密码
     */
    public String login(String username, String password, Model model){
        //1.获取subject对象
        Subject subject = SecurityUtils.getSubject();
        //2.封装用户数据
        if(!subject.isAuthenticated()){
            //首先判断用户是否登陆
            //如果没有登陆的执行下面
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            //3.执行登陆方法
            try{
                subject.login(token);
                //登陆成功
                return "redirect:index/toIndex";
            }catch (UnknownAccountException e){
                model.addAttribute("message","用户名不存在");
                return "login";
            }catch (IncorrectCredentialsException e) { //c.密码错误
                model.addAttribute("message", "密码错误");
                return "login";
            }
        }
        return "redirect:index/toIndex";
    }
    @RequestMapping("/loginOut")
    public String loginOut() {
        return "login";
    }
}
