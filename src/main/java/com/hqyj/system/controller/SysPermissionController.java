package com.hqyj.system.controller;


import com.hqyj.system.model.SysPermission;
import com.hqyj.system.model.SysRole;
import com.hqyj.system.model.SysRolePermission;
import com.hqyj.system.service.ISysPermissionService;
import com.hqyj.utils.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLOutput;
import java.util.List;

/**
 * <p>
 * 权限; InnoDB free: 11264 kB 前端控制器
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Controller
@RequestMapping("/sys-permission")
public class SysPermissionController extends BaseController {

    /**
     * 列表
     *
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        System.out.println("--------------");
        List<SysPermission> sysPermission_db = iSysPermissionService.list();
        model.addAttribute("sysPermission_db", sysPermission_db);
        return "view/system/permission/permissionList";
    }

    /**
     * 增加UI
     *
     * @param model
     * @return
     */
    @RequestMapping("/addUI")
    public String addUI(Model model) {
        //1.准备父级节点
        List<SysPermission> sysPermission_db = iSysPermissionService.list();
        model.addAttribute("sysPermission_db", sysPermission_db);

        return "view/system/permission/addPermission";
    }

    /**
     * 增加
     *
     * @param model
     * @param sysPermission
     * @param pIdSTR
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model, SysPermission sysPermission, String pIdSTR) {
        int pId = Integer.parseInt(pIdSTR);
        sysPermission.setPId(pId);
        Boolean b = iSysPermissionService.save(sysPermission);
        //-----.
        List<SysPermission> sysPermission_db = iSysPermissionService.list();
        model.addAttribute("sysPermission_db", sysPermission_db);
        return "view/system/permission/permissionList";
    }

    /**
     * 删除
     *
     * @param model
     * @param sysPermission
     * @param permissionIdStr
     * @return
     */
    @RequestMapping("/delete")
    public String delete(Model model, SysPermission sysPermission, String permissionIdStr) {
        int permissionId = Integer.parseInt(permissionIdStr);
        sysPermission.setPId(permissionId);
        System.out.println("-----" + permissionId);
        boolean b = iSysPermissionService.deleteSysPermissionByPermissionId(permissionId);
        //-----.
        List<SysPermission> sysPermission_db = iSysPermissionService.list();
        model.addAttribute("sysPermission_db", sysPermission_db);
        return "view/system/permission/permissionList";
    }

    /**
     * 修改UI
     *
     * @param model
     * @param sysPermission
     * @param permissionIdStr
     * @return
     */
    @RequestMapping("/updateUI")
    public String updateUI(Model model, SysPermission sysPermission, String permissionIdStr) {
        int permissionId = Integer.parseInt(permissionIdStr);
        sysPermission.setPId(permissionId);
        System.out.println("-----" + permissionId);
        //调用业务
        //1.准备被修改数据
        SysPermission sysPermission_db = iSysPermissionService.getById(permissionId);
        model.addAttribute("sysPermission_db", sysPermission_db);

        //2.准备父级节点数据
        List<SysPermission> sysPermission_dbList = iSysPermissionService.list();
        model.addAttribute("sysPermission_dbList", sysPermission_dbList);

        return "view/system/permission/updatePermission";
    }

    @RequestMapping("/update")
    public String update(Model model, SysPermission sysPermission, String permissionIdStr, String pIdSTR) {
        //字符类型转数字类型
        int permissionId = Integer.parseInt(permissionIdStr);
        int pId = Integer.parseInt(pIdSTR);
        //维护业务逻辑
        sysPermission.setPermissionId(permissionId);
        sysPermission.setPId(pId);
        //--》调用业务
        Boolean b = iSysPermissionService.saveOrUpdate(sysPermission);

        if (b == true) {
            model.addAttribute("message", "增加成功");
        } else {
            model.addAttribute("message", "增加失败");
        }

        //-----.
        List<SysPermission> sysPermission_db = iSysPermissionService.list();
        model.addAttribute("sysPermission_db", sysPermission_db);
        return "view/system/permission/permissionList";
    }


    @RequestMapping("/FPPermissionUI")
    public String FPPermissionUI(Model model, SysRole sysRole, String roleIdStr) {
        int roleId = Integer.parseInt(roleIdStr);
        //1.准备角色数据
        SysRole sysRole_db = iSysRoleService.getById(roleId);
        model.addAttribute("sysRole_db", sysRole_db);
        //2.准备权限列表数据
        List<SysPermission> sysPermission_dbLiist = iSysPermissionService.list();
        model.addAttribute("sysPermission_dbLiist", sysPermission_dbLiist);

        return "view/system/permission/FPPermission";
    }


    @RequestMapping("/FPPermission")
    public String FPPermission(Model model, SysRole sysRole, SysPermission sysPermission, SysRolePermission sysRolePermission, String roleIdStr, String[] permissionIdStrs) {
        int roleId = Integer.parseInt(roleIdStr);
        System.out.println("-------1-------" + roleId);

        //通过roleId，先删除
        Boolean m = iSysRolePermissionService.deleteSysRolePermissionByRoleId(roleId);

        for (String permissionIdStr : permissionIdStrs) {
            int permissionId = Integer.parseInt(permissionIdStr);
            System.out.println("-------2-------" + permissionId);
            //调用业务
            sysRolePermission.setRoleId(roleId);
            sysRolePermission.setPermissionId(permissionId);
            Boolean b = iSysRolePermissionService.save(sysRolePermission);
        }

        //----
        //1.准备角色数据
        SysRole sysRole_db = iSysRoleService.getById(roleId);
        model.addAttribute("sysRole_db", sysRole_db);
        //2.准备权限列表数据
        List<SysPermission> sysPermission_dbLiist = iSysPermissionService.list();
        model.addAttribute("sysPermission_dbLiist", sysPermission_dbLiist);
        return "view/system/permission/FPPermission";
    }

}

