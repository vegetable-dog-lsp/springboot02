package com.hqyj.system.controller;


import com.hqyj.system.model.SysRole;
import com.hqyj.system.model.SysUser;
import com.hqyj.system.model.SysUserRole;
import com.hqyj.system.service.ISysRoleService;
import com.hqyj.system.service.ISysUserService;
import com.hqyj.utils.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 角色表; InnoDB free: 11264 kB 前端控制器
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Controller
@RequestMapping("/sys-role")
public class SysRoleController extends BaseController {
    /**
     * 列表
     *
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<SysRole> sysRoleList_db = iSysRoleService.queryRoleList();
        model.addAttribute("sysRoleList_db", sysRoleList_db);
        return "view/system/role/roleList";
    }

    /**
     * 增加UI
     *
     * @param model
     * @return
     */
    @RequestMapping("/addUI")
    public String addUI(Model model) {
        return "view/system/role/addRole";
    }

    /**
     * 增加
     *
     * @param model
     * @param sysRole
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model, SysRole sysRole) {
        System.out.println(sysRole);
        boolean b = iSysRoleService.save(sysRole);
        //------
        List<SysRole> sysRoleList_db = iSysRoleService.queryRoleList();
        model.addAttribute("sysRoleList_db", sysRoleList_db);
        return "view/system/role/roleList";
    }

    /**
     * 删除
     *
     * @param model
     * @param sysRole
     * @param roleIdStr
     * @return
     */
    @RequestMapping("/delete")
    public String delete(Model model, SysRole sysRole, String roleIdStr) {
        int roleId = Integer.parseInt(roleIdStr);
        boolean b = iSysRoleService.removeById(roleId);
        //------
        List<SysRole> sysRoleList_db = iSysRoleService.queryRoleList();
        model.addAttribute("sysRoleList_db", sysRoleList_db);
        return "view/system/role/roleList";
    }

    /**
     * 修改UI
     *
     * @param model
     * @param sysRole
     * @param roleIdStr
     * @return
     */
    @RequestMapping("/updateUI")
    public String updateUI(Model model, SysRole sysRole, String roleIdStr) {
        int roleId = Integer.parseInt(roleIdStr);
        System.out.println("-------------" + roleId);
        SysRole sysRole_db = iSysRoleService.getById(roleId);
        model.addAttribute("sysRole_db", sysRole_db);
        return "view/system/role/updateRole";
    }

    /**
     * 修改
     *
     * @param model
     * @param sysRole
     * @param roleIdStr
     * @return
     */
    @RequestMapping("/update")
    public String update(Model model, SysRole sysRole, String roleIdStr) {
        int roleId = Integer.parseInt(roleIdStr);
        sysRole.setRoleId(roleId);
        boolean b = iSysRoleService.saveOrUpdate(sysRole);
        //------
        List<SysRole> sysRoleList_db = iSysRoleService.queryRoleList();
        model.addAttribute("sysRoleList_db", sysRoleList_db);
        return "view/system/role/roleList";
    }

    @RequestMapping("/FPRoleUI")
    public String FPRoleUI(Model model, SysRole sysRole, String roleIdStr, String userIdStr) {
        int userId = Integer.parseInt(userIdStr);
        //业务：
        //1.准备用户信息
        SysUser sysUser_db = iSysUserService.getById(userId);
        model.addAttribute("sysUser_db", sysUser_db);
        //2.准备分配角色信息
        List<SysRole> sysRoleList_db = iSysRoleService.queryRoleList();
        model.addAttribute("sysRoleList_db", sysRoleList_db);

        //------
        return "view/system/role/FPRole";
    }

    @RequestMapping("/FPRole")
    public String FPRole(Model model, SysRole sysRole, SysUserRole sysUserRole, String roleIdStr, String userIdStr) {
        int userId = Integer.parseInt(userIdStr);
        int roleId = Integer.parseInt(roleIdStr);
        System.out.println("------userId--------" + userId);
        System.out.println("--------roleId------" + roleId);
        //业务：
        //--》先判断数据库有没有
        Boolean m = iSysUserRoleService.deleteSysUserRoleByUserId(userId);
        //保存
        sysUserRole.setUserId(userId);
        sysUserRole.setRoleId(roleId);
        boolean b = iSysUserRoleService.save(sysUserRole);

        //------
        //1.准备用户信息
        SysUser sysUser_db = iSysUserService.getById(userId);
        model.addAttribute("sysUser_db", sysUser_db);
        //2.准备分配角色信息
        List<SysRole> sysRoleList_db = iSysRoleService.queryRoleList();
        model.addAttribute("sysRoleList_db", sysRoleList_db);
        return "view/system/role/FPRole";
    }
}


