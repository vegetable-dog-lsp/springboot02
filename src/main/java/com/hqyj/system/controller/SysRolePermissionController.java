package com.hqyj.system.controller;


import com.hqyj.utils.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色权限表; InnoDB free: 11264 kB 前端控制器
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@RestController
@RequestMapping("/sys-role-permission")
public class SysRolePermissionController extends BaseController {

}

