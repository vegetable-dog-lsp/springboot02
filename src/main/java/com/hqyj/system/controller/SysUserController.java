package com.hqyj.system.controller;


import com.hqyj.system.model.SysUser;
import com.hqyj.system.service.ISysUserService;
import com.hqyj.utils.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 用户表; InnoDB free: 11264 kB 前端控制器
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Controller
@RequestMapping("/sys-user")
public class SysUserController extends BaseController {

    /**
     * 列表
     *
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<SysUser> sysUserList_db = iSysUserService.queryUserList();
        model.addAttribute("sysUserList_db", sysUserList_db);
        return "view/system/user/userList";
    }

    /**
     * 增加UI
     *
     * @param model
     * @return
     */
    @RequestMapping("/addUI")
    public String addUI(Model model) {
        System.out.println("-----------addUI----------");
        return "view/system/user/addUser";
    }

    /**
     * 增加
     *
     * @param model
     * @param request
     * @param sysUser
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model, HttpServletRequest request, SysUser sysUser) {
        //1.接受数据
        System.out.println(sysUser);
        //3.调用业务
        boolean b = iSysUserService.save(sysUser);
        //2.跳转页面
        List<SysUser> sysUserList_db = iSysUserService.queryUserList();
        model.addAttribute("sysUserList_db", sysUserList_db);
        return "view/system/user/userList";
    }

    /**
     * 删除
     *
     * @param model
     * @param request
     * @param sysUser
     * @param userIdStr
     * @return
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request, SysUser sysUser, String userIdStr) {
        //1.接受数据
        System.out.println(userIdStr);
        //字符转数字类型
        int userId = Integer.parseInt(userIdStr);
        //3.调用业务
        boolean b = iSysUserService.removeById(userId);
        //2.跳转页面
        List<SysUser> sysUserList_db = iSysUserService.queryUserList();
        model.addAttribute("sysUserList_db", sysUserList_db);
        return "view/system/user/userList";
    }

    /**
     * 修改UI
     *
     * @param model
     * @param request
     * @param sysUser
     * @param userIdStr
     * @return
     */
    @RequestMapping("/updateUI")
    public String updateUI(Model model, HttpServletRequest request, SysUser sysUser, String userIdStr) {
        //1.接受数据
        System.out.println(userIdStr);
        //字符转数字类型
        int userId = Integer.parseInt(userIdStr);
        //3.调用业务
        SysUser sysUser_db = iSysUserService.getById(userId);
        model.addAttribute("sysUser_db", sysUser_db);
        //2.跳转页面
        return "view/system/user/updateUser";
    }

    @RequestMapping("/update")
    public String update(Model model, HttpServletRequest request, SysUser sysUser, String userIdStr) {
        //1.接受数据
        System.out.println(userIdStr);
        //字符转数字类型
        int userId = Integer.parseInt(userIdStr);
        sysUser.setUserId(userId);
        System.out.println(sysUser);

        //3.调用业务
        Boolean b = iSysUserService.saveOrUpdate(sysUser);

        //2.跳转页面
        List<SysUser> sysUserList_db = iSysUserService.queryUserList();
        model.addAttribute("sysUserList_db", sysUserList_db);
        return "view/system/user/userList";
    }
}

