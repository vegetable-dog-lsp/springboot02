package com.hqyj.system.mapper;

import com.hqyj.system.model.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 权限; InnoDB free: 11264 kB Mapper 接口
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {
    @Delete("DELETE FROM sys_permission WHERE permission_id=#{permissionId}")
    boolean deleteSysPermissionByPermissionId(int permissionId);
    @Select("SELECT\n" +
            "sys_permission.permission_id,\n" +
            "sys_permission.p_id,\n" +
            "sys_permission.permission_code,\n" +
            "sys_permission.permission_name,\n" +
            "sys_permission.icon,\n" +
            "sys_permission.url,\n" +
            "sys_permission.type,\n" +
            "sys_permission.state\n" +
            "FROM\n" +
            "sys_user_role\n" +
            "INNER JOIN sys_role ON sys_user_role.role_id = sys_role.role_id\n" +
            "INNER JOIN sys_role_permission ON sys_role.role_id = sys_role_permission.role_id\n" +
            "INNER JOIN sys_permission ON sys_role_permission.permission_id = sys_permission.permission_id\n" +
            "WHERE user_id=#{userId}")
    List<SysPermission> querySysPermissionListByUserId(Integer userId);
}
