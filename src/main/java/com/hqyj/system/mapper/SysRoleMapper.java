package com.hqyj.system.mapper;

import com.hqyj.system.model.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 角色表; InnoDB free: 11264 kB Mapper 接口
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
        @Select("SELECT\n" +
                "sys_role.role_id,\n" +
                "sys_role.role_name,\n" +
                "sys_role.role_code\n" +
                "FROM\n" +
                "sys_user_role\n" +
                "INNER JOIN sys_role ON sys_user_role.role_id = sys_role.role_id\n" +
                "WHERE user_id=#{userId}")
    List<SysRole> querySysRoleListByUserId(Integer userId);
}
