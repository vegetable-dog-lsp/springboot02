package com.hqyj.system.mapper;

import com.hqyj.system.model.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;

/**
 * <p>
 * 角色权限表; InnoDB free: 11264 kB Mapper 接口
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {
    @Delete("DELETE FROM sys_role_permission WHERE role_id=#{roleId}")
    Boolean deleteSysRolePermissionByRoleId(int roleId);
}
