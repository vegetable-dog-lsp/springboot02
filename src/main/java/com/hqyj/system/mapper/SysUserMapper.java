package com.hqyj.system.mapper;

import com.hqyj.system.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 用户表; InnoDB free: 11264 kB Mapper 接口
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

@Select("select * from sys_user where username = #{username}")
    SysUser querySysUserByUsername(String username);
}
