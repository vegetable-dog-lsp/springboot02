package com.hqyj.system.mapper;

import com.hqyj.system.model.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;

/**
 * <p>
 * 用户角色表; InnoDB free: 11264 kB Mapper 接口
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    @Delete("DELETE FROM sys_user_role WHERE user_id=#{userId}")
    Boolean deleteSysUserRoleByUserId(int userId);
}
