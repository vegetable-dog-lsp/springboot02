package com.hqyj.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户角色表; InnoDB free: 11264 kB
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserRole implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户角色ID
     */
    @TableId(value = "user_role_id", type = IdType.AUTO)
    private Integer userRoleId;

    /**
     * 主键ID
     */
    private Integer userId;

    /**
     * 角色ID
     */
    private Integer roleId;


}
