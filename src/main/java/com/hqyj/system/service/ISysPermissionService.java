package com.hqyj.system.service;

import com.hqyj.system.model.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 权限; InnoDB free: 11264 kB 服务类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface ISysPermissionService extends IService<SysPermission> {

    boolean deleteSysPermissionByPermissionId(int permissionId);

    List<SysPermission> querySysPermissionListByUserId(Integer userId);
}
