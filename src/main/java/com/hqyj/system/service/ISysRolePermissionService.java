package com.hqyj.system.service;

import com.hqyj.system.model.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限表; InnoDB free: 11264 kB 服务类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

    Boolean deleteSysRolePermissionByRoleId(int roleId);
}
