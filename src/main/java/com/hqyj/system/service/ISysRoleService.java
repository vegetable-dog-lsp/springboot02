package com.hqyj.system.service;

import com.hqyj.system.model.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表; InnoDB free: 11264 kB 服务类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface ISysRoleService extends IService<SysRole> {

    List<SysRole> queryRoleList();

    List<SysRole> querySysRoleListByUserId(Integer userId);
}
