package com.hqyj.system.service;

import com.hqyj.system.model.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表; InnoDB free: 11264 kB 服务类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

    Boolean deleteSysUserRoleByUserId(int userId);
}
