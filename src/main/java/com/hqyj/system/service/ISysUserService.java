package com.hqyj.system.service;

import com.hqyj.system.model.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表; InnoDB free: 11264 kB 服务类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
public interface ISysUserService extends IService<SysUser> {

    List<SysUser> queryUserList();

    SysUser querySysUserByUsername(String username);
}
