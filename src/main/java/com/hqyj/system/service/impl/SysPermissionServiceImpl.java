package com.hqyj.system.service.impl;

import com.hqyj.system.model.SysPermission;
import com.hqyj.system.mapper.SysPermissionMapper;
import com.hqyj.system.service.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限; InnoDB free: 11264 kB 服务实现类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {
    @Autowired
    public SysPermissionMapper sysPermissionMapper;

    @Override
    public boolean deleteSysPermissionByPermissionId(int permissionId) {
        return sysPermissionMapper.deleteSysPermissionByPermissionId(permissionId);
    }

    @Override
    public List<SysPermission> querySysPermissionListByUserId(Integer userId) {
        return sysPermissionMapper.querySysPermissionListByUserId(userId);
    }


}
