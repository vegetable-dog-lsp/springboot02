package com.hqyj.system.service.impl;

import com.hqyj.system.model.SysRolePermission;
import com.hqyj.system.mapper.SysRolePermissionMapper;
import com.hqyj.system.service.ISysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表; InnoDB free: 11264 kB 服务实现类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Override
    public Boolean deleteSysRolePermissionByRoleId(int roleId) {
        return sysRolePermissionMapper.deleteSysRolePermissionByRoleId(roleId);
    }
}
