package com.hqyj.system.service.impl;

import com.hqyj.system.model.SysRole;
import com.hqyj.system.mapper.SysRoleMapper;
import com.hqyj.system.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色表; InnoDB free: 11264 kB 服务实现类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public List<SysRole> queryRoleList() {
        return sysRoleMapper.selectList(null);
    }

    @Override
    public List<SysRole> querySysRoleListByUserId(Integer userId) {
        return sysRoleMapper.querySysRoleListByUserId(userId);
    }
}
