package com.hqyj.system.service.impl;

import com.hqyj.system.model.SysUserRole;
import com.hqyj.system.mapper.SysUserRoleMapper;
import com.hqyj.system.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表; InnoDB free: 11264 kB 服务实现类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public Boolean deleteSysUserRoleByUserId(int userId) {
        return sysUserRoleMapper.deleteSysUserRoleByUserId(userId);
    }
}
