package com.hqyj.system.service.impl;

import com.hqyj.system.model.SysUser;
import com.hqyj.system.mapper.SysUserMapper;
import com.hqyj.system.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户表; InnoDB free: 11264 kB 服务实现类
 * </p>
 *
 * @author zlf
 * @since 2021-12-15
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
    //注入SysUserMapper代理接口
    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public List<SysUser> queryUserList() {
        return sysUserMapper.selectList(null);
    }

    @Override
    public SysUser querySysUserByUsername(String username) {
        return sysUserMapper.querySysUserByUsername(username);
    }
}
