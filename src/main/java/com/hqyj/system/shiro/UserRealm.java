package com.hqyj.system.shiro;

import com.hqyj.system.model.SysPermission;
import com.hqyj.system.model.SysRole;
import com.hqyj.system.model.SysUser;
import com.hqyj.system.service.ISysPermissionService;
import com.hqyj.system.service.ISysRoleService;
import com.hqyj.system.service.ISysUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;

/**
 *
 */
public class UserRealm extends AuthorizingRealm {
    @Autowired
    public ISysUserService iSysUserService;
    @Autowired
    public ISysRoleService iSysRoleService;
    @Autowired
    public ISysPermissionService iSysPermissionService;
    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //1.拿到用户名
        String username = (String) token.getPrincipal();
        //2.查询数据库
        SysUser sysUser_db = iSysUserService.querySysUserByUsername(username);
        //3.shiro判断逻辑，用户名密码
        //3.1判断用户名
        if (sysUser_db == null) {
            return null;//Shiro底层抛出 UnknownAccountException异常
        }
        //3.2判断密码
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(sysUser_db,
                sysUser_db.getPassword(), ByteSource.Util.bytes(sysUser_db.getSalt()), this.getName());
        return authenticationInfo;
    }

    //2.授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        //1.创建Shiro授权对象
        SimpleAuthorizationInfo authorization = new SimpleAuthorizationInfo();
        //2.获取认证用户信息
        SysUser sysUser = (SysUser) principalCollection.getPrimaryPrincipal();
        //用户ID
        int userId = sysUser.getUserId();

        //3.用户查询：用户相关的角色(roleListXZList)
        HashSet<String> roles = new HashSet<>();
        List<SysRole> sysRoleXZList = iSysRoleService.querySysRoleListByUserId(userId);
        for (SysRole sysRole : sysRoleXZList) {
            roles.add(sysRole.getRoleCode());
        }

        //4.用户查询：用户相关的权限(firstLvMenuList)
        HashSet<String> permissions = new HashSet<>();
        List<SysPermission> sysPermissionXZList = iSysPermissionService.querySysPermissionListByUserId(userId);
        for (SysPermission sysPermission : sysPermissionXZList) {
            permissions.add(sysPermission.getPermissionCode());
        }

        //5.创建一个权限对象，收集角色和权限信息
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRoles(roles);
        info.addStringPermissions(permissions);
        return info;
    }

}
