package com.hqyj.utils;

import com.hqyj.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Auther: ZLF
 * @Date: 2021/12/17 10:58
 * @Description:
 */
public class BaseController {
    @Autowired
    public ISysUserService iSysUserService;
    @Autowired
    public ISysRoleService iSysRoleService;
    @Autowired
    public ISysPermissionService iSysPermissionService;
    @Autowired
    public ISysUserRoleService iSysUserRoleService;
    @Autowired
    public ISysRolePermissionService iSysRolePermissionService;
}